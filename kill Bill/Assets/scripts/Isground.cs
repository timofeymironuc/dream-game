using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Isground : MonoBehaviour
{
    public GameObject main_object;
    public bool is_ground = false;

    void OnTriggerEnter(Collider collision){
        is_ground = true;
        main_object.GetComponent<Move>().ground = true;
        print("cround in");
    }
    
    void OnTriggerExit(Collider collision){
        is_ground = false;
        main_object.GetComponent<Move>().ground = false;
        print("ground out");
    }
}
