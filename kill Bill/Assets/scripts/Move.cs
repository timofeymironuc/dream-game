using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Move : MonoBehaviour
{
    public float Speed = 2.0f;
    public float Jump = 1.0f;

    public bool ground = false;

    public Rigidbody rb;
    public GameObject Camera;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {

        //move
        Vector3 move = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        move = transform.TransformDirection(move);
        move.y=rb.velocity.y;
        rb.MovePosition(transform.position + move * Time.deltaTime * Speed);

        if (Input.GetAxis("Jump")>0 && ground){
            rb.AddForce(Vector3.up * Jump);
        }

        //rotate player x
        gameObject.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0));

        //rotate camera y
        Camera.transform.Rotate(new Vector3(Mathf.Clamp(Camera.transform.rotation.x - Input.GetAxis("Mouse Y"), 0, 45) - Camera.transform.rotation.x, 0, 0));
    }
}