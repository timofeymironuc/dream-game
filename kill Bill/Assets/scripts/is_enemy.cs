using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class is_enemy : MonoBehaviour
{
    void OnCollisionEnter(Collision collision){
        if(collision.gameObject.tag == "enemy"){
            if(collision.collider.name == "hit_box"){
                print("Kill");
                GameObject Second_enemy = GameObject.Instantiate(collision.gameObject);
                Second_enemy.GetComponent<Rigidbody>().MovePosition(new Vector3(Random.Range(-5, 5), 5, Random.Range(-5, 5)));
                Destroy(collision.gameObject);
            }
            else{
                Destroy(gameObject);
                print("Dead");
            }
        }
    }
}
